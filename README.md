![输入图片说明](https://www.niucloud.com/img/readme/%E9%A1%B6%E9%83%A8%E5%B9%BF%E5%91%8A1.jpg)



 :point_up:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond: :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:
:small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:
<br>
如果对您有帮助，您可以点右上角 “ **Star** ” 收藏一下 ，获取第一时间更新，谢谢！
<br>
 :point_up: :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond: :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:
:small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:  :small_blue_diamond:  :small_orange_diamond:  :small_blue_diamond:
<br>

### niucloud-admin是什么？
niucloud-admin是一款快速开发SaaS通用管理系统后台框架，前端采用最新的技术栈Vite+TypeScript+Vue3+ElementPlus最流行技术架构，后台结合PHP8、Java SDK、Python等主流后端语言搭建，内置集成Saas多站点、多租户套餐、用户权限、代码生成器、表单设计、云存储、短信发送、素材中心、微信及公众号、支付宝小程序、Api模块一系列开箱即用功能，是一款快速可以开发企业级应用的软件系统。

### niucloud-admin产品特性说明

1.niucloud-admin 软件开发框架内置各种基础功能，可大量节省SaaS系统开发周期，快速完成交付。

2.Saas架构底层设计，支持单端模式开发部署，支持Saas多站点，多用户，多租户模式研发，支持无限账号，无需重复部署安装，高效管理多个用户，多个应用。

3.插件设计的管理机制，支持第三方开发者参与开发各种功能插件，独特的软件插件架构设计，支持多插件并存，即便二次开发之后，也不会影响与主框架同步无缝升级。

4.内置集成Saas多站点、多租户套餐、用户权限、代码生成器、表单设计、云存储、短信发送、素材中心、微信及公众号、支付宝小程序、Api模块一系列开箱即用功能，是一款快速可以开发企业级应用的软件系统。

5.框架支持在线更新，可统一管理主框架及各种插件的在线安装，在线更新。

6.niucloud-admin是一款真正的二次开发神器！


### niucloud-admin技术说明

- 后台php采用thinkphp6+php8+mysql,支持composer快速安装扩展，支持redis缓存以及消息队列，支持多语言设计开发，同时开发采用严格的restful的api设计开发。

- 后台前后端分离采用element-plus、vue3.0、typescript、vite、pina等前端技术,同时使用i18n支持国际化多语言开发。

- 手机端采用uniapp前后端分离，同时使用uview、vue3.0、typescript、vite、pina等前端技术，同时使用i18n支持国际化多语言开发，可以灵活编译成h5,微信小程序，支付宝小程序，抖音小程序等使用场景。

- niucloud-admin采用多租户的saas系统设计，能够提供企业级软件服务运营 ，同时满足用户多站点，多商户，多门店等系统开发需求。

- niucloud-admin结合当前市面上很多框架结构不规范，导致基础结构不稳定等情况，严格定义了分层设计的开发规范，同时api接口严格采用restful的开发规范，能够满足大型业务系统或者微服务的开发需求。

- niucloud-admin前端以及后端采用严格的多语言开发规范，包括前端展示，api接口返回，数据验证，错误返回等全部使用多语言设计规范，使开发者能够真生意义上实现多语言的开发需求。

- niucloud-admin已经搭建好常规系统的开发底层，具体的底层功能包括：管理员管理，权限管理，网站设置，计划任务管理，素材管理，会员管理，会员账户管理，微信公众号以及小程序管理，支付管理，第三方登录管理，消息管理，短信管理，文章管理，前端装修等全面的基础功能，这样开发者不需要开发基础的结构而专心开发业务。

- niucloud-admin系统内置支持微信/支付宝支付，微信公众号/小程序/短信消息管理，阿里云/腾讯云短信，七牛云/阿里云存储等基础的功能扩展，后续会根据实际业务不断扩展基础组件。

- niucloud-admin结合系统结构特点专门开发了代码生成器，这样开发者根据数据表就可以一键生成基础的业务代码，包括：后台php业务代码以及对应的前端vue代码。

- 前端采用标准的element-plus，开发者不需要详细了解前端，只需要用标准的element组件就可以。

- 手机端设计开发了自定义装修，同时提供了基础的开发组件，方便开发者设计开发手机自定义页面装修的开发需求。

- 手机端使用uniapp ，同时使用uview页面展示，可以开发出丰富的手机样式，同时不需要专门学习小程序，app等开发语言，只需要通过uniapp编译就可以。

### 操作指南
 :fa-th-list:  [官网地址](https://www.niucloud.com)
 | [服务市场]()
 | [系统功能]()
 | [系统演示](https://demo.niucloud.com/web/)
 | [使用手册](https://www.kancloud.cn/niucloud/niucloud-admin-develop/3153336)
 | [二开手册](https://www.kancloud.cn/niucloud/niucloud-admin-develop/3153336)
 | [论坛地址](https://www.niushop.com/web/community/index.html)
 | [留言评论](https://www.niushop.com/web/community/index.html)

### 演示地址
- 站点后台演示网址：[<a href='https://demo.niucloud.com/admin/' target="_blank"> 查看 </a>]       
<a href='https://demo.niucloud.com/admin/' target="_blank">https://demo.niucloud.com/admin/</a>  账号：test  密码：123456
- 平台后台演示网址：[<a href='https://demo.niucloud.com/admin/' target="_blank"> 查看 </a>]       
<a href='https://demo.niucloud.com/admin/' target="_blank">https://demo.niucloud.com/admin/</a>  账号：admin  密码：123456

- 前端演示二维码

![输入图片说明](https://www.niucloud.com/img/readme/%E6%BC%94%E7%A4%BA%E5%89%8D%E7%AB%AF-new.png)

### 安装部署
#### 宝塔部署
- 1.环境要求<br/>
php 8.0  <br/>    mysql5.6及以上  <br/>      启用redis   <br/>      Nignx/Apache
- 2.登录网站【<a href='https://gitee.com/niucloud-team/niucloud-admin.git' target="_blank">https://gitee.com/niucloud-team/niucloud-admin/</a>】下载框架源码。
- 3.源码放置到宝塔根目录，访问域名/niucloud/public/index.php，进入安装界面，点击下一步，输入数据库安装信息，下一步完成安装。详细教程查看 :arrow_right: <a href='https://www.kancloud.cn/niucloud/niucloud-admin-develop/3148343' target="_blank">【开发手册】 :arrow_left: </a>
- 4.redis配置：<br/>
 ①安装redis，如下图：
![输入图片说明](https://www.niucloud.com/img/readme/%E5%AE%9D%E5%A1%94%E5%AE%89%E8%A3%85redis.png)
②安装php的扩展redis，如下图所示：
![输入图片说明](https://www.niucloud.com/img/readme/php%E6%89%A9%E5%B1%95%E5%AE%89%E8%A3%85redis%E6%89%A9%E5%B1%95.png)
③将redis密码填入到niucloud/.env文件中，如下图：
![输入图片说明](https://www.niucloud.com/img/readme/%E9%85%8D%E7%BD%AEredis%E5%AF%86%E7%A0%81.png)

- 5.配置伪静态<br/>
可直接复制下方代码：
Nginx配置

```html
location / {
    if (!-e $request_filename) {
        rewrite  ^(.*)$  /index.php/$1  last;
     break;
   }
}
```

apache配置
```
<IfModule mod_rewrite.c>
Options +FollowSymlinks -Multiviews
  RewriteEngine On
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php?/$1 [QSA,PT,L]
  RewriteEngine on RewriteCond % !^$
</IfModule>
```
粘贴位置如下图：
![输入图片说明](https://www.niucloud.com/img/readme/%E6%B7%BB%E5%8A%A0%E4%BC%AA%E9%9D%99%E6%80%81%E4%BD%8D%E7%BD%AE.png)

#### docker快速部署
- 1.启动docker，打开终端输入命令行，回车执行命令。

```
docker run -d --name niucloudadmin_php -p 20221:80 niucloud/niucloudadmin_php:1.6.0
```
- 2.访问配置的虚拟域名或者localhost或者127.0.0.1/niucloud/public/index.php即可进入安装界面。

#### 前端运行
- 1.拉取代码

```
git clone https://gitee.com/niucloud-team/niucloud-admin.git
```
- 2.安装依赖

```
cd niucloud-admin template
npm install
```

- 3.本地运行

```
npm run dev
```

- 4.前端代码打包

```
npm run build
```

快来添加属于你的专属客服吧~

![输入图片说明](https://www.niucloud.com/img/readme/%E6%B7%BB%E5%8A%A0%E5%AE%A2%E6%9C%8D-old.png)
#### 扫描下方二维码加入niucloud-admin开发者交流群
![输入图片说明](https://www.niucloud.com/img/readme/%E5%BC%80%E5%8F%91%E8%80%85%E4%BA%A4%E6%B5%81%E7%BE%A4-old.png)

### 框架管理端部分页面展示

- 管理端控制台页面，可直观体现站点会员数量、平台拥有的站点数、所有访客数量统计；同时也加入了统计走势图，便于平台根据需求制定或者调整运营策略；主要快捷入口可方便快速打开需要处理的业务数据
![输入图片说明](https://www.niucloud.com/img/readme/%E6%8E%A7%E5%88%B6%E5%8F%B0.png)
- 系统性设置，可助力平台更好的运营
![输入图片说明](https://www.niucloud.com/img/readme/%E7%9F%AD%E4%BF%A1%E3%80%81%E6%8F%90%E7%8E%B0%E8%AE%BE%E7%BD%AE.png)
![输入图片说明](https://www.niucloud.com/img/readme/%E7%BD%91%E7%AB%99%E3%80%81%E7%89%88%E6%9D%83%E8%AE%BE%E7%BD%AE.png)
![输入图片说明](https://www.niucloud.com/img/readme/%E5%AD%98%E5%82%A8%E3%80%82.png)
- 文章模块管理页面展示
![输入图片说明](https://www.niucloud.com/img/readme/%E6%96%87%E7%AB%A0%E5%88%97%E8%A1%A8.png)
- 可自动生成代码
![输入图片说明](https://www.niucloud.com/img/readme/%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90.png)
- 素材管理
![输入图片说明](https://www.niucloud.com/img/readme/%E7%B4%A0%E6%9D%90%E7%AE%A1%E7%90%86.png)

### 开源使用须知

1.允许用于个人学习、毕业设计、教学案例、公益事业、商业使用;

2.本框架应用源代码所有权和著作权归niucloud官方所有，基于niucloud-admin框架开发的应用，所有权和著作权归应用开发商所有。但必须声明是基于niucloud-admin框架开发，请自觉遵守，否则产生的一切任何后果责任由侵权者自负;

3.本框架源码全部开源;包括前端，后端，无任何加密;

4.商用请仔细审查代码和漏洞，不得用于任一国家许可范围之外的商业应用，产生的一切任何后果责任自负;

5.一切事物有个人喜好的标准，本开源代码意在分享，不喜勿喷。

### 战略合作伙伴
![输入图片说明](https://www.niucloud.com/img/readme/%E5%90%88%E4%BD%9C%E4%BC%99%E4%BC%B4.png)

### MIT开源协议官方声明
![输入图片说明](https://img.kancloud.cn/05/b3/05b34f6e22d0a0c1535928e3ce4b209e_1700x2338.jpg)

### 版权信息
版权所有Copyright  2015-2035 niucloud-admin 版权所有
All rights reserved。

杭州牛之云科技有限公司 提供技术支持